	package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;
import domain.Account;


@Controller
public class Transfer {
	@RequestMapping("/inicio")
	String inicio(){
		return "transfer";
	}
	
	@RequestMapping("/transferir")
	String transferir(@RequestParam String cuenta1, @RequestParam String cuenta2, @RequestParam monto){
		AccountRepository repository = new InMemoryAccountRepository();
		TransferService service = new TransferService(repository);
		Account a1 = new Account("1001", 550);
		Account a2 = new Account("1002", 50);
		System.out.println(a1);
		System.out.println(a2);
		repository.save(a1);
		repository.save(a2);
		// el monto est en string y para la funcion te pude double para la tranferencia
	}
}
